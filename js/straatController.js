app.controller("straatCtrl", function ($scope, $http, $ngConfirm, intranetService) {

    var _selectedWijk = null;
    var _selectedWaarde = null;

    var _data;

    $scope.active = null;

    $scope.propertyName = 'Straat_naam';
    $scope.reverse = false;

    $scope.gemeentes = intranetService.getGemeentes();
    $scope.Gemeente_id = 0;
    $scope.gemeenteNaam = "";

    $scope.sortBy = function (propertyName) {
        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
    };

    $scope.checkAction = function () {
        if ($scope.active.Id === 0) {
            $ngConfirm('Gelieve eerst de nieuwe straat verder af te werken!');
        } else {
            $ngConfirm('Gelieve eerste de huidige bewerking verder af te werken!');
        }
    };

    $scope.getStraten = function (e) {
        if ($scope.active !== null) {
            $scope.checkAction();
        } else {
            intranetService
                .getAll(e, "Straten")
                .then(function (result) {
                    $scope.data = result;
                    _data = angular.copy($scope.data);

                    $scope.Gemeente_id = angular.copy(e.g_id);
                    $scope.gemeenteNaam = e.naam;

                    $scope.sortBy('Straat_naam');
                    $scope.reverse = false;

                });
        }
    };

    $scope.getGemeente = function () {
        var gemeente = {
            'g_id': $scope.Gemeente_id,
            'naam': $scope.gemeenteNaam
        };
        return gemeente;
    };

    $scope.getWijkagenten = function () {
        intranetService
            .getAll($scope.getGemeente(), "Agenten")
            .then(function (result) {
                $scope.agenten = result;
                $scope.waardes = ["Even", "Oneven", "Even+Oneven"];

            });
    };

    $scope.resetStraten = function () {
        if ($scope.active !== null) {
            $scope.checkAction();
        } else {
            $scope.searchText = "";
            $scope.Gemeente_id = 0;
            $scope.data = null;
            $scope.reverse = false;
            $scope.gemeenteNaam = "";
            $scope.resetValues();
        }
    };

    $scope.resetValues = function () {
        $scope.selected = null;
        _selectedWijk = null;

        $scope.selectedWaarde = null;
        _selectedWaarde = null;

        $scope.searchText = "";
        $scope.active = null;

        $scope.sortBy('Straat_naam');
        $scope.reverse = false;
    };

    $scope.addRow = function (Gemeente_id) {

        if (!Gemeente_id || Gemeente_id === 0) {
            $ngConfirm('U moet eerst een gemeente selecteren!');
        } else if ($scope.active !== null) {
            $scope.checkAction();
        } else {
            var straat = {
                'Id': 0,
                'Straat_naam': null,
                'Huisnr_van': null,
                'Huisnr_tot': null,
                'Even_oneven': null,
                'Wijk': null
            };

            $scope.resetValues();
            $scope.sortBy('Id');

            $scope.data.splice(0, 0, straat);
            $scope.getWijkagenten();

            $scope.active = straat;
        }
    };

    $scope.editRow = function (r) {
        if ($scope.active !== null) {
            $scope.checkAction();
        } else {
            $scope.active = r;

            $scope.selected = {
                'Wijk': r.Wijk,
                'Agent': r.Agent
            };
            _selectedWijk = angular.copy(r.Wijk);
            $scope.searchText = r.Straat_naam;
            $scope.selectedWaarde = r.Even_oneven;
            _selectedWaarde = angular.copy(r.Even_oneven);

            $scope.getWijkagenten();
        }
    };

    $scope.cancelRow = function (r) {
        $ngConfirm({
            title: 'Wijzigingen annuleren?',
            content: 'Dit scherm zal verdwijnen als er geen actie volgt',
            autoClose: 'cancel|8000',
            buttons: {
                cancelStraat: {
                    text: 'Wijziging annuleren',
                    btnClass: 'btn-red',
                    action: function () {
                        $scope.resetValues();

                        $scope.data = angular.copy(_data);

                        $ngConfirm('De wijzigingen werden teniet gedaan!');
                    }
                },
                cancel: function () {
                    $ngConfirm('Wijzigingen verder zetten!');
                }
            }
        });
    };

    $scope.updateWijk = function (wijk) {
        _selectedWijk = wijk;
    };

    $scope.updateWaarde = function (waarde) {
        _selectedWaarde = waarde;
    };

    $scope.getData = function (straat) {
        var data = {
            'Id': straat.Id,
            'Gemeente_id': $scope.Gemeente_id,
            'Straat_naam': straat.Straat_naam,
            'Huisnr_van': straat.Huisnr_van,
            'Huisnr_tot': straat.Huisnr_tot,
            'Even_oneven': _selectedWaarde,
            'Wijk': _selectedWijk
        };
        return data;
    };

    $scope.saveRow = function (straat) {
        var saved = false;

        var straat2 = $scope.getData(straat);

        saved = intranetService.save(straat2, "Straat");
        if (saved) {
            $scope.active = null;
            $ngConfirm('De doorgevoerde wijzigingen werden opgeslagen!');

        } else {
            $ngConfirm('Er ging iets mis. Wijzigingen werd niet opgeslagen!');
        }
        $scope.getStraten($scope.getGemeente());
        $scope.sortBy('Straat_naam');
        $scope.reverse = false;
    };

    $scope.deleteRow = function (straat) {

        if ($scope.active !== null) {
            $scope.checkAction();
        } else {
            $ngConfirm({
                title: 'Straat verwijderen?',
                content: 'Dit scherm zal verdwijnen als er geen actie volgt',
                autoClose: 'annuleren|8000',
                buttons: {
                    deleteUser: {
                        text: 'Verwijderen',
                        btnClass: 'btn-red',
                        action: function () {
                            intranetService
                                .delete(straat, "Straat")
                                .then(function (antwoord) {
                                    var deleted = false;
                                    deleted = antwoord;
                                    if (deleted) {
                                        $ngConfirm('De straat werd verwijderd uit de database!');
                                        $scope.getStraten($scope.getGemeente());
                                        $scope.sortBy('Straat_naam');
                                        $scope.reverse = false;
                                    } else {
                                        $ngConfirm('Er ging iets mis. De straat werd niet verwijderd...');
                                    }
                                });
                        }
                    },
                    annuleren: function () {
                        $ngConfirm('Aanvraag werd geannuleerd');
                    }
                }
            });
        }
    };
});

var app = angular.module("MyApp", ['cp.ngConfirm', 'ngMessages']);

app.directive('stringToNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (value) {
                return '' + value;
            });
            ngModel.$formatters.push(function (value) {
                return parseFloat(value);
            });
        }
    };
});

app.service('intranetService', function ($http, $sce, $q) {
    var domain = "https://intranet.jaspieland.eu/";

    this.getGemeentes = function () {
        return [{
            'g_id': '1',
            'naam': "Beringen"
                }, {
            'g_id': '2',
            'naam': "Ham"
                }, {
            'g_id': '3',
            'naam': "Tessenderlo"
                }];
    };

    this.getHoogste = function () {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: domain + 'getMaxWijkJson.php'
        }).then(function successCallback(response) {
            deferred.resolve(response.data);
        }).catch(function (e) {
            deferred.reject(e);
        });
        return deferred.promise;
    };

    this.getAll = function (gemeente, bestandsnaam) {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            params: {
                Gemeente_id: gemeente.g_id
            },
            url: domain + 'get' + bestandsnaam + 'Json.php'
        }).then(function successCallback(response) {
            deferred.resolve(response.data);
        }).catch(function (e) {
            deferred.reject(e);
        });
        return deferred.promise;
    };

    this.save = function (object, bestandsnaam) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            data: object,
            url: domain + 'save' + bestandsnaam + 'Json.php',
            headers: {
                'Content-Type': undefined
            }
        }).then(function successCallback(response) {
            deferred.resolve(true);
        }).catch(function (e) {
            deferred.reject(e);
        });
        return deferred.promise;
    };

    this.delete = function (object, bestandsnaam) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            data: object,
            url: domain + 'delete' + bestandsnaam + 'Json.php',
            headers: {
                'Content-Type': undefined
            }
        }).then(function successCallback(response) {
            deferred.resolve(true);
        }).catch(function (e) {
            deferred.reject(e);
        });
        return deferred.promise;
    };
});

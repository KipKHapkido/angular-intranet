app.controller("wijkCtrl", function ($scope, $ngConfirm, intranetService) {
    
    var _data;

    $scope.active = null;
    
    $scope.propertyName = 'Agent';
    $scope.reverse = false;
    
    $scope.gemeentes = intranetService.getGemeentes();
    $scope.Gemeente_id = 0;
    $scope.gemeenteNaam = "";
    
    $scope.max = 100;

    $scope.hoogste = function () {
        intranetService
                .getHoogste()
                .then(function (result) {
                    $scope.max = result;
                    console.log(result);
                });
    }

    $scope.sortBy = function (propertyName) {
        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
    };

    $scope.checkAction = function () {
        if ($scope.active.Id === 0) {
            $ngConfirm('Gelieve eerst de nieuwe wijkagent verder af te werken!');
        } else {
            $ngConfirm('Gelieve eerste de huidige bewerking verder af te werken!');
        }
    };

    $scope.getWijkagenten = function (e) {
        
        if ($scope.active !== null) {
            $scope.checkAction();
        } else {
            //$scope.$apply(function () {
            intranetService
                .getAll(e, "Agenten")
                .then(function (result) {
                    $scope.data = result;
                    _data = angular.copy($scope.data);
                    
                    $scope.Gemeente_id = angular.copy(e.g_id);
                    $scope.gemeenteNaam = e.naam;
                    $scope.sortBy('Agent');
                    $scope.reverse = false;
                    
                });
            // });
        }
    };

    $scope.resetStraten = function () {
        if ($scope.active !== null) {
            $scope.checkAction();
        } else {
            $scope.searchText = "";
            $scope.Gemeente_id = 0;
            $scope.data = null;
            _data = null;
            $scope.reverse = false;
            $scope.gemeenteNaam = "";
            $scope.resetValues();
        }
    };

    $scope.resetValues = function () {
        $scope.active = null;
        $scope.reverse = false;
    };

    $scope.addRow = function (Gemeente_id) {

        if (!Gemeente_id || Gemeente_id === 0) {
            $ngConfirm('U moet eerst een gemeente selecteren!');
        } else if ($scope.active !== null) {
            $scope.checkAction();
        } else {
            $scope.hoogste();            
            var agent = {
                'Id': 0,
                'Wijk': $scope.max,
                'Agent': null,
                'Foto': null,
                'tel_nr': null,
                'email': null
            };    
            
            $scope.resetValues();
            $scope.sortBy('Id');

            $scope.data.splice(0, 0, agent);

            $scope.active = agent;
        }
    };

    $scope.editRow = function (r) {
        if ($scope.active !== null) {
            $scope.checkAction();
        } else {
            $scope.max = 100;
            $scope.active = r;
        }

    };

    $scope.cancelRow = function (r) {
        $ngConfirm({
            title: 'Wijzigingen annuleren?',
            content: 'Dit scherm zal verdwijnen als er geen actie volgt',
            autoClose: 'cancel|8000',
            buttons: {
                cancelStraat: {
                    text: 'Wijziging annuleren',
                    btnClass: 'btn-red',
                    action: function () {
                        $scope.resetValues();

                        $scope.data = angular.copy(_data);

                        $ngConfirm('De wijzigingen werden teniet gedaan!');
                    }
                },
                cancel: function () {
                    $ngConfirm('Wijzigingen verder zetten!');
                }
            }
        });
    };

    $scope.getData = function (agent) {
        var data = {
            'Id': agent.Id,
            'Wijk': agent.Wijk,
            'Agent': agent.Agent,
            'Foto': agent.Foto,
            'tel_nr': agent.tel_nr,
            'email': agent.email
        };
        return data;
    };

    $scope.getGemeente = function() {
        var gemeente =  {
            'g_id': $scope.Gemeente_id,
            'naam': $scope.gemeenteNaam
        };
        return gemeente;
    };
    
    $scope.saveRow = function (agent) {

        var saved = false;

        saved = intranetService.save(agent, "Agent");
        if (saved) {
            $scope.active = null;
            $ngConfirm('De doorgevoerde wijzigingen werden opgeslagen!');

            $scope.getWijkagenten($scope.getGemeente());
            $scope.sortBy('Wijk');
            $scope.reverse = false;
        } else {
            $ngConfirm('Er ging iets mis. Wijzigingen werd niet opgeslagen!');
        }

    };

    $scope.deleteRow = function (agent) {
        
        if ($scope.active !== null) {
            $scope.checkAction();
        } else {
            $ngConfirm({
                title: 'Wijkagent verwijderen?',
                content: 'Dit scherm zal verdwijnen als er geen actie volgt',
                autoClose: 'annuleren|8000',
                buttons: {
                    deleteUser: {
                        text: 'Verwijderen',
                        btnClass: 'btn-red',
                        action: function () {
                            intranetService
                                .delete(agent, "Agent")
                                .then(function (antwoord) {
                                    var deleted = false;
                                    deleted = antwoord;
                                    if (deleted) {
                                        $ngConfirm('De wijkagent werd verwijderd uit de database!');
                                        $scope.getWijkagenten($scope.getGemeente());
                                        $scope.sortBy('Agent');
                                        $scope.reverse = false;
                                    } else {
                                        $ngConfirm('Er ging iets mis. De wijkagent werd niet verwijderd...');
                                    }
                                });
                        }
                    },
                    annuleren: function () {
                        $ngConfirm('Aanvraag werd geannuleerd');
                    }
                }
            });
        }
        
    };
});
